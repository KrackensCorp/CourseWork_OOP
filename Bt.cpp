#include "Bt.h"

B_tree <int> 

my_bt(7);  // создание корня бинарного дерева


// Заполнение дерева целыми значениями (статические элементы)
void populate() {
    my_bt.add(5);
    my_bt.add(5);
    my_bt.add(9);
    my_bt.add(6);
    my_bt.add(4);
    my_bt.add(11);
    my_bt.add(8);
    my_bt.add(19);
    my_bt.add(2);
    my_bt.add(10);
}

int main() {
    populate();
    my_bt.print();
    cout << endl
    << "---------------------------------"         << endl
    << "|Инкапсуляция \t\t    YES\t|"              << endl
    << "|Наследование \t\t    NO\t|"               << endl
    << "|3 класса, 1 абстрактный    NO\t|"         << endl
    << "|Полиморфизм \t\t    NO\t|"                << endl
    << "|Деструкторы \t\t    YES\t|"               << endl
    << "|Конструкторы \t\t    YES\t|"              << endl
    << "|Перегрузка \t\t    NO\t|"                 << endl
    << "|Списки инициализации \t    NO\t|"         << endl
    << "|Дружественные функции \t    YES\t|"       << endl
    << "|Статические элементы \t    YES\t|"        << endl
    << "|Классы \t\t    YES\t|"                    << endl
    << "---------------------------------"         << endl
    << endl;
    cout << "До сортировки:  \t";
    my_bt.print_pre_order();
    cout << "После сортировки: \t";
    my_bt.print_post_order();
    cout << "In-order:   \t\t";
    my_bt.print_in_order();
    return 0;
}