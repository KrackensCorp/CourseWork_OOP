#include <iostream>
#include <cassert>

using namespace std;

template <class T>
class B_tree
{
  private:
    struct T_node
    {
        friend class B_tree;
        T val;               // данные узла
        T_node *left;        // указатель на левый узел
        T_node *right;       // указатель на правый узел
        int count;           // число повторов val при вводе
        T_node();            // конструктор

        T_node(const T node_val) : val(node_val) {} // конструктор
        ~T_node() {}                                // деструктор
        // печать данных в виде дерева "на боку" с корнем слева
        // "Обратный" рекурсивный обход (т.е. справа налево)
        // Листья показаны как "NULL".
        void print(const int level = 0) const
        { // Инициализация указателем this (а не корнем)
            // это позволяет пройти по любому поддереву
            const T_node *tn = this;
            if (tn)
                tn->right->print(level + 1); // сдвиг вправо до листа
            for (int n = 0; n < level; ++n)
                cout << "      ";
            if (tn)
                cout << tn->val << '(' << tn->count << ')' << endl;
            else
                cout << "NULL" << endl;
            if (tn)
                tn->left->print(level + 1); // сдвиг на левый узел
        }
    };

  private:
    T_node *root;
    T_node *zero_node;
    //  Запретить копирование и присваивание
    B_tree(const B_tree &);
    B_tree &operator=(const B_tree &);
    // Создать корневой узел и проинициализировать его
    void new_root(const T root_val)
    {
        root = new T_node(root_val);
        root->left = 0;
        root->right = 0;
        root->count = 1;
    }
    // Find_node(T find_value) возвращает ссылку на
    // указатель для упрощения реализации   remove(T).
    T_node *&find_node(T find_value)
    {
        T_node *tn = root;
        while ((tn != 0) && (tn->val != find_value))
        {
            if (find_value < tn->val)
                tn = tn->left; // движение налево
            else
                tn = tn->right; // движение направо
        }
        if (!tn)
            return zero_node;
        else
            return tn;
    }
    // Присоединяет новое значение ins_val к соответствующему листу,
    // если значения нет в дереве, и увеличивает count для каждого
    // пройденного узла
    T_node *insert_node(const T ins_val, T_node *tn = 0)
    {
        if (!root)
        {
            new_root(ins_val);
            return root;
        }
        if (!tn)
            tn = root;

        if ((tn) && (tn->val != ins_val))
        {
            if (ins_val < tn->val)
            {
                if (tn->left) // просмотр левого поддерева
                    insert_node(ins_val, tn->left);
                else
                {
                    attach_node(tn, tn->left, ins_val); // вставка узла
                    return tn->left;
                }
            }
            else
            {
                if (tn->right) // просмотр правого поддерева
                    insert_node(ins_val, tn->right);
                else
                {
                    attach_node(tn, tn->right, ins_val); // вставка узла
                    return tn->right;
                }
            }
        }
        else if (tn->val == ins_val)
            add_count(tn, 1);
        assert(tn); // Оценивает выражение и, когда результат ЛОЖЕН, печатает
                    // диагностическое сообщение и прерывает программу
        return 0;
    }
    // Создание нового листа и его инициализация
    void attach_node(T_node *new_parent,
                     T_node *&new_node, T insert_value)
    {
        new_node = new T_node(insert_value);
        new_node->left = 0;
        new_node->right = 0;
        new_node->count = 1;
    }
    // Увеличение числа повторов содержимого узла
    void add_count(T_node *tn, int incr)
    {
        tn->count += incr;
    }
    // Удаление всех узлов дерева в обходе с отложенной
    // выборкой. Используется в   ~B_tree().
    void cleanup(T_node *tn)
    {
        if (!tn)
            return;
        if (tn->left)
        {
            cleanup(tn->left);
            tn->left = 0;
        }
        if (tn->right != 0)
        {
            cleanup(tn->right);
            tn->right = 0;
        }
        delete tn;
    }
    // рекурсивно печатает значения в поддереве с корнем  tn
    //  (обход дерева с предварительной выборкой)
    void print_pre(const T_node *tn) const
    {
        if (!tn)
            return;
        cout << tn->val << "  ";
        if (tn->left)
            print_pre(tn->left);
        if (tn->right)
            print_pre(tn->right);
    }
    // рекурсивно печатает значения в поддереве с корнем  tn
    //  (обход дерева )
    void print_in(const T_node *tn) const
    {
        if (!tn)
            return;
        if (tn->left)
            print_in(tn->left);
        cout << tn->val << "  ";
        if (tn->right)
            print_in(tn->right);
    }
    // рекурсивно печатает значения в поддереве с корнем  tn
    //  (обход дерева с отложенной выборкой)
    void print_post(const T_node *tn) const
    {
        if (!tn)
            return;
        if (tn->left)
            print_post(tn->left);
        if (tn->right)
            print_post(tn->right);
        cout << tn->val << "  ";
    }

  public:
    B_tree() : zero_node(0) { root = 0; }
    B_tree(const T root_val) : zero_node(0)
    {
        new_root(root_val);
    }
    virtual ~B_tree()     //СОЙДЕТ ЗА ВИРТУАЛЬНУЮ ФУНКЦИЮ и последствие абстрактный класс?)***********************
    {
        cleanup(root);
    }
    // Добавляет к дереву через функцию insert_node() значение, если его
    // там еще нет. Возвращает true, если элемент добавлен, иначе false
    bool add(const T insert_value)
    {
        T_node *ret = insert_node(insert_value);
        if (ret)
            return true;
        else
            return false;
    }
    // Find(T) возвращает true, если find_value найдено
    // в дереве, иначе false
    bool find(T find_value)
    {
        T_node *tn = find_node(find_value);
        if (tn)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    // Print() производит обратную порядковую выборку
    // и печатает дерево "на боку"
    void print() const
    {
        cout << "\n \t Бинарное дерево \n" << endl;
        // Это вызов    Binary_tree::Tree_node::print( ),
        // а не рекурсивный вызов  Binary_tree::print( ).
        root->print();
    }
    // последовательная печать дерева при обходе
    // с предварительной, порядковой и отложенной выборкой.
    // Вызываются рекурсивные private-функции, принимающие
    // параметр Tree_node *
    void print_pre_order() const
    {
        print_pre(root);
        cout << endl;
    }
    void print_in_order() const
    {
        print_in(root);
        cout << endl;
    }
    void print_post_order() const
    {
        print_post(root);
        cout << endl;
    }
};