#include "btree.h"
#include <cstdlib>
#include <iostream>

#ifndef COLOR_H
#define COLOR_H
#define RESET "\033[0m"
#define GREEN "\033[1;32m"
//#define RED   "\033[1;31m"
#define GRAY "\033[34m"

using namespace std;

int main()
{
	//system("clear");
	//system("cls");
	cout << endl
		 << endl;

	BTree *root = new BTree(20);

	root->insertion(11);
	root->insertion(25);
	root->insertion(6);
	root->insertion(19);
	root->insertion(24);
	root->insertion(30);
	root->insertion(2);
	root->insertion(27);
	root->insertion(28);
	root->insertion(22);
	root->insertion(35);
	root->insertion(21);
	root->insertion(36);
	root->insertion(13);
	root->insertion(15);
	root->insertion(12);
	cout << GRAY << "__________________Первоначальное дерево_____________________" << RESET << endl;
	root->print();
	cout << endl;
	cout << endl;

	cout << GRAY << "_________________Добавим эллементы: 18, 14, 23_______________" << RESET << endl
		 << endl;
	root->insertion(18);
	root->insertion(14);
	root->insertion(23);
	root->print();
	cout << endl;
	cout << endl;

	cout << GRAY << "_________________Уберем эллементы: 6, 13, 20_________________" << RESET << endl
		 << endl;
	root->delete_node(6);
	root->delete_node(13);
	root->delete_node(20);
	root->print();

	cout << endl
		 << "---------------------------------" << endl
		 << "|Инкапсуляция \t\t    YES\t|" << endl
		 << "|Наследование \t\t    YES\t|" << endl
		 << "|3 класса \t\t    YES\t|" << endl
		 << "|1 абстрактный \t\t    YES\t|" << endl
		 << "|Полиморфизм \t\t    YES\t|" << endl
		 << "|Деструкторы \t\t    YES\t|" << endl
		 << "|Конструкторы \t\t    YES\t|" << endl
		 << "|Перегрузка \t\t    YES\t|" << endl
		 << "|Интерфейс \t\t    YES\t|" << endl
		 << "|Статические элементы \t    YES\t|" << endl
		 << "---------------------------------" << endl
		 << endl;
	return 0;
}
#endif