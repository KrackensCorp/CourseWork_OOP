#include <iostream>
#include <cstdlib>

#ifndef BINARY_TREE_H
#define BINARY_TREE_H
#define RESET "\033[0m"
#define GREEN "\033[1;32m"
//#define RED   "\033[1;31m"
//#define GRAY  "\033[47m"

using namespace std;

// Интенфейс
/*static*/ class Collection // (Абстрактный класс)
{
	virtual void insertion(int key) = 0; // Виртуальная функция
};

// Главная структура (Абстрактный класс)
class BinaryTree
{
  protected: // инкапсуляция
	struct Node
	{
		int key;
		struct Node *left;
		struct Node *right;

		Node() // Перегрузка конструктора
		{
			this->key = 0;
			this->left = NULL;
			this->right = NULL;
		}
		// Список инициализации
		Node(int key) : left(NULL), right(NULL) //Перегрузка
		{
			this->key = key;
		}

		~Node() {} // Деструктор

		void print()
		{
			cout << "(" << GREEN << this->key << RESET << ")<" << endl;
		}

	} * root;

  public:
	virtual void delete_node(int key) = 0; //Виртуальная функция (Полиморфизм)
	void print()
	{
		int lvl = 3;
		Node *root = this->root;
		print(root, lvl);
		cout << endl;
	}

	void print(Node *node, int lvl)
	{
		if (node != NULL)
		{
			print(node->right, lvl + 1);
			if (node == root)
			{
				cout << "Дерево:          ";
			}

			for (int i = 0; i < lvl && node != this->root; i++)
			{
				cout << "       ";
			}

			node->print();
			print(node->left, lvl + 1);
		}
	}
};

// binary tree
class BTree : public BinaryTree, Collection //наследование
{
  public:
	BTree()
	{
		this->root = new Node();
	}

	BTree(int key)
	{
		this->root = new Node(key);
	}

	~BTree() {} // Деструктор

	void insertion(int key)
	{
		if (this->root == NULL)
		{
			return;
		}

		Node *node = this->root;
		Node *newNode, *parent = nullptr;

		// Поиск листьев узла
		while (node != NULL)
		{
			parent = node;
			if (key < node->key)
			{
				node = node->left;
			}
			else if (key > node->key)
			{
				node = node->right;
			}
			else
			{
				break;
			}
		}

		// Вставка
		newNode = new Node(key);
		if (key < parent->key)
		{
			parent->left = newNode;
		}
		else
		{
			parent->right = newNode;
		}
	}

	// Удаление узла
	void delete_node(int key) // Полиморфизм
	{
		Node *node = this->root;
		Node *parent = NULL;
		while (node->key != key)
		{
			if (key < node->key)
			{
				parent = node;
				node = node->left;
			}
			else if (key > node->key)
			{
				parent = node;
				node = node->right;
			}
			else
			{
				break;
			}
		}

		if (this->root->key == key)
		{
			Node *ptr = this->root->right;
			Node *parent = this->root;
			if (ptr != NULL)
			{
				if (ptr->left != NULL)
				{
					if (ptr->left != NULL)
					{
						while (ptr->left)
						{
							if (parent->key == this->root->key)
							{
								parent = parent->right;
							}
							else
							{
								parent = parent->left;
							}
							ptr = ptr->left;
						}
						Node *left = this->root->left;
						Node *right = this->root->right;
						Node *ptr_right = ptr->right;
						ptr->left = left;
						ptr->right = right;
						if (ptr_right == NULL)
						{
							parent->left = NULL;
						}
						else
						{
							parent->left = ptr_right;
						}
						this->root = ptr;
					}
					else
					{
						Node *ptr_left = this->root->left;
						free(root);
						this->root = ptr->left;
					}
				}
				else
				{
					Node *left = this->root->left;
					this->root = this->root->right;
					this->root->left = left;
				}
			}
			else
			{
				Node *del = this->root;
				this->root = this->root->left;
				free(del);
			}
		}
		else if (node->left && node->right && node != this->root) // Оба дочерних узла
		{
			Node *ptr = node->right;
			Node *parent_ptr = NULL;
			Node *left = node->left;
			Node *right = node->right;

			if (node->right->left != NULL)
			{
				while (ptr->left)
				{
					parent_ptr = ptr;
					ptr = ptr->left;
				}

				if (parent->left == node)
				{
					if (ptr->right != NULL)
					{
						parent_ptr->left = ptr->right;
					}
					else
					{
						parent_ptr->left = NULL;
					}
					ptr->left = left;
					ptr->right = right;
					parent->left = ptr;
				}
				else
				{
					if (ptr->right != NULL)
					{
						parent_ptr->left = ptr->right;
					}
					else
					{
						parent_ptr->left = NULL;
					}
					ptr->left = left;
					ptr->right = right;
					parent->right = ptr;
				}
			}
			else
			{
				if (parent->left == node)
				{
					parent->left = node->right;
					parent->left->left = node->left;
				}
				else
				{
					parent->right = node->right;
					parent->right->left = node->left;
				}
			}
			free(node);
		}
		else if (node->left && !node->right && key != this->root->key) // Только правый дочерний узел
		{
			if (parent->right == node)
			{
				parent->right = node->left;
			}
			else
			{
				parent->left = node->left;
			}
		}
		else if (!node->left && node->right) // Только левый дочерний узел
		{
			if (parent->right == node)
			{
				parent->right = node->right;
			}
			else
			{
				parent->left = node->right;
			}
			free(node);
		}
		else if (!node->left && !node->right) // Отстутствие дочерних узлов
		{
			if (parent->left == node)
			{
				parent->left = NULL;
			}
			else
			{
				parent->right = NULL;
			}
			free(node);
		}
	}
};
#endif